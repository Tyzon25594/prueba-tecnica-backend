﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ApiTiendaPrueba.Modelo;

namespace ApiTiendaPrueba.Data
{
    public class ApiTiendaPruebaContext : DbContext
    {
        public ApiTiendaPruebaContext (DbContextOptions<ApiTiendaPruebaContext> options)
            : base(options)
        {
        }

        public DbSet<ApiTiendaPrueba.Modelo.Producto> Producto { get; set; }
    }
}
